#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include "header.h"
#include "SDL.h"
// main function where I can embed SFML and interface
const int WIDTH = 640; // WIDTH and HEIGHT of the window
const int HEIGHT = 480;
SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;

int main(int argc, char *argv[])
{
  int steps = 0;
  int stepsCounter = 0;
  printf("Enter how many steps/generations you would like to generate for the cells: \n");
  scanf(" %i", &steps);
  steps = validateInput(steps); // get our initial number of steps from here
  FILE* fp;
  loadGrid(fp); //load the grid from file

  atexit(Shutdown);
  if(!Initialize())
  {
    exit(1);
  }

  while(stepsCounter <= steps) // keep looping until we have reached the max number of generations
  {
    Update();
    if(stepsCounter == steps)
    {
      break;
    }
    checkGrid(Grid,rows, columns); // once displayed on the render for the previous generation call checkGrid to check for next gen grid
    addGenerationArray(generationArray); // add the next generation ready to be displayed by the next cal to update
    stepsCounter++;
    sleep(2);
  }
  if(saveGrid(fp))
  {
    printf("Final state has been saved to file named 'output.txt' \n");
  }

  bool quit = false;
  SDL_Event event;

  while(!quit)
  {
    while (SDL_PollEvent(&event))
    {
      if(event.type == SDL_QUIT)
      {
        quit = true;
      }
    }
  }
  SDL_Quit();
}
