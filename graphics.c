#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include "header.h"
#include "SDL.h"
// file containing all SDL related functions to display the graphical interface

bool Initialize(void)
{
  if(SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    fprintf(stderr, "Failed to initialize SDL: %s\n", SDL_GetError());
    return false;
  }
  window = SDL_CreateWindow("Game Of Life", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
  if(!window)
  {
    return false;
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  SDL_SetRenderDrawColor(renderer,255,255,255,255);
  SDL_RenderClear(renderer); // above 2 lines set the background colour
  if(!window)
  {
    return false;
  }
  return true;
}

void Update()
{
  SDL_Rect Graphics[MaxRow][MaxCol]; //
  double Rec_Width = WIDTH / columns; // must be double say they had 11 columns 640/11 is a decimal
  double Rec_Height = HEIGHT / rows; // so essentially whatever number of rows and columns the user decides to use the width and height of each cell will fit into the window length proportionaly
  double columnAdder = 0;
  for(int i=0; i<rows; i++)
  {
    double rowAdder = 0; // set to 0 for the next row
    for(int j=0; j<columns; j++)
    {
      if(Grid[i][j] == 0) // if a cell is dead make it white
      {
        SDL_SetRenderDrawColor(renderer,255,255,255,255);
        Graphics[i][j].x = (WIDTH / columns) - Rec_Width + rowAdder;
        Graphics[i][j].y = (HEIGHT / rows) - Rec_Height + columnAdder;
        Graphics[i][j].w = Rec_Width;
        Graphics[i][j].h = Rec_Height;
        SDL_RenderFillRect(renderer, &Graphics[i][j]);
        SDL_SetRenderDrawColor(renderer,192,192,192,192);
        SDL_RenderDrawRect(renderer, &Graphics[i][j]); // this line and the above line are used to just draw the silver borders to distinguish the cells
      }
      if(Grid[i][j] == 1) // if a cell is living make it black
      {
        SDL_SetRenderDrawColor(renderer,0,0,0,255);
        Graphics[i][j].x = (WIDTH / columns) - Rec_Width + rowAdder;
        Graphics[i][j].y = (HEIGHT / rows) - Rec_Height + columnAdder;
        Graphics[i][j].w = Rec_Width;
        Graphics[i][j].h = Rec_Height;
        SDL_RenderFillRect(renderer, &Graphics[i][j]);
        SDL_SetRenderDrawColor(renderer,192,192,192,192);
        SDL_RenderDrawRect(renderer, &Graphics[i][j]);
      }
      rowAdder += Rec_Width; // increment after each column of this row
    }
    columnAdder += Rec_Height; // increment after each row so we drop to the colum below
  }
  SDL_RenderPresent(renderer); // Use this function to update the screen with any rendering performed since the previous call.

}

void Shutdown(void) // ensures the program is properly shutdown
{
  if(renderer)
  {
    SDL_DestroyRenderer(renderer);
  }
  if(window)
  {
    SDL_DestroyWindow(window);
  }
  SDL_Quit();
}
