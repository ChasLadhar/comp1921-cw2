This folder contains all the required files needed to successfully compile this program.

Git Repository URL: https://gitlab.com/ChasLadhar/comp1921-cw2.git
IMPORTANT when creating an array in the input.txt file you must follow the guidelines:

1.) Only 1's and 0's are allowed
2.) There can be no incomplete missing values from columns and rows
3.) The max array size(grid size) you can create is 100x100 as I am limited to a finite world
4.) The number of rows and columns can be of different sizes (i.e rectangular) as long as all values for each row and column have a 0 or 1

Finally it is IMPORTANT that you also place the input.txt file and output.txt file in the build folder when you build!

Then cd build, make .. , make in order to build.
Then to execute the unit tests enter ./Unit_tests in the build folder
To execute the main project enter ./Life

ENJOY!
