#include "unity.h"
#include "header.h"
// file containing all test functions  for the program

void test_loadGrid()
{
  FILE *fp;
  int x = 0;
  x = loadGrid(fp); // should return a 1
  TEST_ASSERT_FALSE_MESSAGE(x == 0, "loadGrid can't deal with null file pointers");
  //loadGrid should return 1 as it can deal with null pointer else x should remain 1
}

void test_saveGrid()
{
  FILE *fp;
  int x = 0;
  x = saveGrid(fp); // should return a 1
  TEST_ASSERT_FALSE_MESSAGE(x == 0, "saveGrid can't deal with null file pointers");
  //saveGrid should return 1 as it can deal with null pointer else x should remain 1
}

void test_checkGrid()
{
  // 3 cases where we will create 3 arrays in the Grid[][] array and then compare
  // the generationArray[][] changed by the checkGrid() to a comparison array I produced here
  // note TEST_ASSERT_EQUAL_ARRAY only works for 1 dimensional arrays so we have to manually compare the arrays.
  int error = 0;
  // Case 1 where live cells are in the middle
  int Case1Grid[MaxRow][MaxCol]={
    {0,0,0,0,0,0},
    {0,0,0,0,0,0},
    {0,0,1,0,0,0},
    {0,0,1,1,0,0},
    {0,1,0,0,0,0},
    {0,0,0,0,0,0}
  };
  // ISSUE THIS checkGrid ONLY CHECKS THE 0 initialized grid in logic.c
  checkGrid(Case1Grid,6,6);
  int Case1Array[MaxRow][MaxCol]={
    {0,0,0,0,0,0},
    {0,0,0,0,0,0},
    {0,0,1,1,0,0},
    {0,1,1,1,0,0},
    {0,0,1,0,0,0},
    {0,0,0,0,0,0}
  };
  for(int i=0; i<6; i++)
  {
    for(int j=0; j<6; j++)
    {
      if(Case1Array[i][j] != generationArray[i][j])
      {
        error++;
      }
    }
  }
  TEST_ASSERT_FALSE_MESSAGE(error == 1, "1st case failed");
  error = 0;
  for(int i=0; i<6; i++)
  {
    for(int j=0; j<6; j++)
    { // 'clear' the generationArray ready for next test
      generationArray[i][j] = 0;
    }
  }

  // Case 2 where live cells are spread but not touching edge
  int Case2Grid[MaxRow][MaxCol]={
    {0,0,0,0,0,0},
    {0,1,0,0,1,0},
    {0,0,1,0,1,0},
    {0,0,1,1,0,0},
    {0,1,0,0,1,0},
    {0,0,0,0,0,0}
  };

  checkGrid(Case2Grid,6,6);
  int Case2Array[MaxRow][MaxCol]={
    {0,0,0,0,0,0},
    {0,0,0,1,0,0},
    {0,1,1,0,1,0},
    {0,1,1,0,1,0},
    {0,0,1,1,0,0},
    {0,0,0,0,0,0}
  };
  for(int i=0; i<6; i++)
  {
    for(int j=0; j<6; j++)
    {
      if(Case2Array[i][j] != generationArray[i][j])
      {
        error++;
      }
    }
  }
  TEST_ASSERT_FALSE_MESSAGE(error == 1, "2nd case failed");
  error = 0;
  for(int i=0; i<6; i++)
  {
    for(int j=0; j<6; j++)
    { // 'clear' the generationArray ready for next test
      generationArray[i][j] = 0;
    }
  }

  // Case 2 where live cells are spread but not touching edge
  int Case3Grid[MaxRow][MaxCol]={
    {1,0,0,0,0,1},
    {0,1,0,0,1,0},
    {0,0,1,0,1,0},
    {0,0,0,1,0,0},
    {0,1,0,0,1,1},
    {1,0,1,0,1,1}
  };

  checkGrid(Case3Grid,6,6);
  int Case3Array[MaxRow][MaxCol]={
    {0,0,0,0,0,0},
    {0,1,0,1,1,1},
    {0,0,1,0,1,0},
    {0,0,1,1,0,1},
    {0,1,1,0,0,1},
    {0,1,0,1,1,1}
  };
  for(int i=0; i<6; i++)
  {
    for(int j=0; j<6; j++)
    {
      if(Case3Array[i][j] != generationArray[i][j])
      {
        error++;
      }
    }
  }
  TEST_ASSERT_FALSE_MESSAGE(error == 1, "3rd case failed");
}

void test_addGenerationArray()
{
  // want to check that the addGenerationArray is cleared at the end of the function
  // so test by loading an array into it and then checking if it cleared

  int error = 0;
  // Make a random case grid to insert into addGenerationArray(), if the function works
  // properly then all output from the generation Array should be 0 after
  int CaseGenerationArray[MaxRow][MaxCol]={
    {0,0,0,1,0,0},
    {0,1,1,0,0,0},
    {0,0,0,1,0,0},
    {0,1,1,1,0,0},
    {0,0,0,0,0,0},
    {0,1,1,0,0,1}
  };
  addGenerationArray(CaseGenerationArray);
  for(int i=0; i<6; i++)
  {
    for(int j=0; j<6; j++)
    {
      if(CaseGenerationArray[i][j] != 0)
      {
        error++;
      }
    }
  }
  TEST_ASSERT_TRUE_MESSAGE(error == 1, "addGeneration test failed");
}

void test_validateInput()
{
  //Case 1 for n integer
  int i = 55;
  i = validateInput(i);
  TEST_ASSERT_TRUE_MESSAGE(i == 55, "test case 1 failed");

}

void setUp() {
	 //this function is called before each test, it can be empty
}

void tearDown() {
	 //this function is called after each test, it can be empty
}

int main() {
	UNITY_BEGIN();

  RUN_TEST(test_checkGrid);
	RUN_TEST(test_loadGrid);
  RUN_TEST(test_saveGrid);
  RUN_TEST(test_addGenerationArray);
  RUN_TEST(test_validateInput);


	return UNITY_END();
}
