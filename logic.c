#include <stdio.h>
#include <stdlib.h>
#include "header.h"
// file containing all logic related game functions

int rows = 0;
int columns = 0;
int Grid[MaxRow][MaxCol];
int generationArray[MaxRow][MaxCol];

int loadGrid(FILE *file)
{
  char c;
  file=fopen("input.txt", "r");
  if(file != NULL)
  {
    // first we need to work out the number of rows and columns in the array
    do
    {
      c = getc(file);
      if(c == '\n')
      {
        rows++;
        continue;
      }
      if(c == EOF)
      {
        break;
      }
      columns++;
    } while(c != EOF);
    columns = columns/rows;

    rewind(file); // sets the file position back to the start of the file
    for(int i=0; i<rows; i++)
    {
      for(int j=0; j<columns; j++ )
      {
        c = getc(file);
        if(c == '\n')
        {
          c = getc(file); // if the next character is the next line one we don't want to store that so we take the next character in the file present
        }
        int a = c - 48; // get C returns the ASCII value so to convert to the integer value we -48
        Grid[i][j] = a;
      }
    }
    fclose(file);
    return 1;
  }
  else
  {
    printf("Sorry but the file can't be opened.\n");
    return 0;
  }
}


int saveGrid(FILE *file)
{
  file=fopen("output.txt", "w");
  if(file != NULL)
  {
    for(int i=0; i<rows; i++)
    {
      for(int j=0; j<columns; j++)
      {
        fprintf(file, "%i", Grid[i][j]);
      }
      fprintf(file, "\n");
    }
    fclose(file);
    return 1;
  }
  else
  {
    printf("Sorry but the file can't be opened.");
    return 0;
  }
}

void checkGrid(int Grid[MaxRow][MaxCol],int Rows, int Columns)
{
  rows = Rows;
  columns = Columns;
  for(int i=0; i<rows; i++)
  {
    for(int j=0; j<columns; j++)
    {
      // 8 possible cases where at least one array element wont be accessible (the edges and corners) plus 1 case where all neighbours are accessible = total 9 if statements
      int sum = 0;
      if(i==0 && j==0) // top left corner
      {
        sum += Grid[i][j+1];
        sum += Grid[i+1][j+1];
        sum += Grid[i+1][j];
      }
      else if(i==0 && j==(columns-1)) // top right corner
      {
        sum += Grid[i+1][j];
        sum += Grid[i+1][j-1];
        sum += Grid[i][j-1];
      }
      else if(i==0) // top edge
      {
        sum += Grid[i][j+1];
        sum += Grid[i+1][j+1];
        sum += Grid[i+1][j];
        sum += Grid[i+1][j-1];
        sum += Grid[i][j-1];
      }
      else if(j==(columns-1)) // right edge
      {
        sum += Grid[i+1][j];
        sum += Grid[i+1][j-1];
        sum += Grid[i][j-1];
        sum += Grid[i-1][j-1];
        sum += Grid[i-1][j];
      }
      else if(i==(rows-1) && j==(columns-1)) // bottom right corner
      {
        sum += Grid[i][j-1];
        sum += Grid[i-1][j-1];
        sum += Grid[i-1][j];
      }
      else if(i==(rows-1)) // bottom edge
      {
        sum += Grid[i][j+1];
        sum += Grid[i][j-1];
        sum += Grid[i-1][j-1];
        sum += Grid[i-1][j];
        sum += Grid[i-1][j+1];
      }
      else if(i==(rows-1) && j==0) // bottom left corner
      {
        sum += Grid[i][j+1];
        sum += Grid[i-1][j];
        sum += Grid[i-1][j+1];
      }
      else if(j==0) // left edge
      {
        sum += Grid[i][j+1];
        sum += Grid[i+1][j+1];
        sum += Grid[i+1][j];
        sum += Grid[i-1][j];
        sum += Grid[i-1][j+1];
      }
      else // not near edge
      {
        sum += Grid[i][j+1];
        sum += Grid[i+1][j+1];
        sum += Grid[i+1][j];
        sum += Grid[i+1][j-1];
        sum += Grid[i][j-1];
        sum += Grid[i-1][j-1];
        sum += Grid[i-1][j];
        sum += Grid[i-1][j+1];
      }
      if(Grid[i][j] ==  1) // if the selected cell is living
      {
        if(sum == 0 || sum == 1) // and has 0 or 1 neighbours
        {
          generationArray[i][j] = 0; // JUST CHANGE THESE BITS FOR GENERATION ARRAY
        }
        if(sum == 2 || sum == 3) // and has 2 or 3 neighbours
        {
          generationArray[i][j] = 1;
        }
        else if(sum > 3) // if the cell has more than 3 neighbours
        {
          generationArray[i][j] = 0;
        }
      }
      else if(Grid[i][j] == 0) // if the selected cell is dead
      {
        if(sum == 3) //if the cell has exactly 3 neighbours
        {
          generationArray[i][j] = 1;
        }
      }
    }
  }
}

void addGenerationArray(int generationArray[MaxRow][MaxCol])
{
  // note integer arrays are naturally have all elements initialized to 0 when defined
  for(int i=0; i<rows; i++)
  {
    for(int j=0; j<columns; j++)
    {
      Grid[i][j] = generationArray[i][j];
    }
  }

  for(int i=0; i<rows; i++)
  {
    for(int j=0; j<columns; j++)
    {
      generationArray[i][j] = 0;
    }
  }
}

int validateInput(int steps)
{
  int validSteps = steps;
  while(validSteps<1)
  {
    printf("Please enter a valid number above 1. \n");
    scanf(" %i", &validSteps);
  }
  return validSteps;
}
