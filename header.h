// header file containing all variables used across files and function prototypes
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "SDL.h"

// The max number of cells in a row
#define MaxRow 100

// The max number of cells in a column
#define MaxCol 100

// A fixed width of the game window when the game launches
extern const int WIDTH;

// A fixed height of the game window when the game launches
extern const int HEIGHT;

// A SDL window pointer to open the window
extern SDL_Window *window;

// An SDL renderer pointer used to load objects onto the renderer
extern SDL_Renderer *renderer;

// global variable which stores the current generation to be displayed
extern int Grid[MaxRow][MaxCol];

// global variable which stores the next generation to be loaded onto the main grid array for the next time cyle
extern int generationArray[MaxRow][MaxCol];

// global variale that store the number of rows when a grid is loaded
extern int rows;

// global variable that stores the number of columns when a grid is loaded
extern int columns;

// function to load a grid from a saved text file into the grid in the program
// returns 1 if successful
int loadGrid(FILE *file);

// function to save the final state of the grid to a text file
// returns 1 if successful
int saveGrid(FILE *file);

// counts all possible neighbours of a cell and changes cells from
// living to dead or vice versa, takes as input the number of rows and columns
// and the grid which it is checking from(this paramater was added for testing purposes primarily)
void checkGrid(int Grid[MaxRow][MaxCol],int rows, int columns);

// adds the next generation of cells to generationArray by checking all possible neighbours and
// deducing if the cells stays alive or not
void addGenerationArray(int generationArray[MaxRow][MaxCol]);

// checks whether the user has inputted a valid input of an integer which determines the number
// of steps/generations the program will display
int validateInput(int input);

// our 3 SDL function prototypes
// Creates the initial game window
bool Initialize();

// Updates the frame by adding to the renderer ready for the next generation
void Update();

// Function used to ensure that the game is fully shutdown before exiting the window
void Shutdown();
